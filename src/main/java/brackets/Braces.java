package brackets;

import java.util.Stack;

public class Braces {
    public static void main(String[] args) {
        Braces braces = new Braces();
        System.out.println(braces.validateBrackets("(fwes)"));
    }

    public Boolean validateBrackets(String s) {
        Stack<Character> brace = new Stack<>();
        char[] chArray = s.toCharArray();
        for (int i = 0; i < s.length(); i++) {
            if (chArray[i] == '(' | chArray[i] == '[' | chArray[i] == '{') {
                brace.push(chArray[i]);
            }
            if (chArray[i] == ')' | chArray[i] == ']' | chArray[i] == '}') {
                if (brace.isEmpty()) {
                    return false;
                } else {
                    if ((brace.peek().equals('(') & chArray[i] == ')') | (brace.peek().equals('[') & chArray[i] == ']') | (brace.peek().equals('{') & chArray[i] == '}')) {
                        brace.pop();
                    }
                }
            }
        }
        if (brace.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }
}
