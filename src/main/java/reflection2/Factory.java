package reflection2;

import reflection2.annotation.CallingTheMethod;
import reflection2.annotation.GenerateUnit;
import reflection2.annotation.Randomize;
import reflection2.entities.Unit;


import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Factory {


    public Unit createUnitByName(String unitClass) {
        try {
            Class c = Class.forName("reflection2.entities." + unitClass);
            if (c.getSuperclass() == Unit.class) {
                Constructor constructor = c.getConstructor();
                Unit unit = (Unit) constructor.newInstance();
                Field[] fields = unit.getClass().getDeclaredFields();
                Arrays.stream(fields).forEach(field -> {
                    field.setAccessible(true);

                    Randomize randomize = field.getAnnotation(Randomize.class);
                    if (randomize != null) {
                        randomizeFields(unit, field, randomize.max(), randomize.min());
                    }

                    GenerateUnit generateUnit = field.getAnnotation(GenerateUnit.class);
                    if (generateUnit != null) {
                        generateUnit(unit, field);
                    }

                    CallingTheMethod callingTheMethod = field.getAnnotation(CallingTheMethod.class);
                    if (callingTheMethod != null) {
                        try {
                            Method declaredMethod = c.getDeclaredMethod(callingTheMethod.nameMethod());
                            declaredMethod.setAccessible(true);
                            field.set(unit, declaredMethod.invoke(unit));
                        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }
                });
                return unit;
            }


        } catch (InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    void generateUnit(Object o, Field field) {
        Random random = new Random();
        int i = random.nextInt(10) + 1;
        if (i == 1) {
            Class<?> aClass = o.getClass();
            try {
                field.set(o, createUnitByName(aClass.getName().substring(aClass.getName().lastIndexOf(".") + 1)));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    void randomizeFields(Object o, Field field, int max, int min) {
        Random random = new Random();
        try {
            field.set(o, min + random.nextInt(max - min));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }


    public List<Unit> getUnits(int count, String unitClass) {
        ArrayList<Unit> units = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            units.add(createUnitByName(unitClass));
        }
        return units;
    }

}

