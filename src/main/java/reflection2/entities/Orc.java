package reflection2.entities;

import reflection2.annotation.CallingTheMethod;
import reflection2.annotation.GenerateUnit;
import reflection2.annotation.GenerateUnitsByList;
import reflection2.annotation.Randomize;
import reflection2.generateUnits.GenerateName;

import java.util.List;

public class Orc extends Unit {
    @CallingTheMethod(nameMethod = "generateName")
    String name;

    @Randomize(min = 1, max = 20)
    int hp;
    @Randomize(min = 1, max = 20)
    int damage;

    @GenerateUnit
    Unit unit;

    @GenerateUnitsByList(unitClass = "Orc")
    List<Orc> OrcList;

    @Override
    public String toString() {
        return "Orc{" +
                "name='" + name + '\'' +
                ", hp=" + hp +
                ", damage=" + damage +
                ", unit=" + unit +
                '}';
    }

    String generateName() {
        GenerateName generateName = new GenerateName();
        return generateName.generateName();
    }

}
