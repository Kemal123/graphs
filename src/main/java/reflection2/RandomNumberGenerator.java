package reflection2;

import reflection2.entities.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Random;
import java.lang.reflect.Field;

public class RandomNumberGenerator {

    public static void main(String[] args) {
        Factory factory = new Factory();
        System.out.println(factory.createUnitByName("Orc"));
        System.out.println(factory.createUnitByName("Elf"));
        System.out.println(factory.createUnitByName("Goblin"));
        System.out.println(factory.getUnits(4, "Orc"));
    }
}