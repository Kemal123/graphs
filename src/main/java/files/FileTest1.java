package files;

import java.io.File;
import java.io.IOException;

//создание папки и файла
public class FileTest1 {

    public static void main(String[] args) {
        String path = "C:\\Users\\hasis\\OneDrive\\desktop\\FileTest1";
        File fileTest = new File(path);

        FileTest1 fileTest1 = new FileTest1();
        fileTest1.method(fileTest);
    }

    void method(File fileTest) {
        if (fileTest.exists()) {

            if (fileTest.isDirectory()) {
                String path = fileTest.getPath() + "\\" + fileTest.getName() + ".txt";
                File file = new File(path);

                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                if (fileTest.isFile()) {
                    String path = fileTest.getPath();
                    path = path.substring(0, path.indexOf('.'));
                    File file = new File(path);
                    file.mkdir();
                }
            }
        }
    }
}
