package files;

import java.io.Serializable;

public class Person implements Serializable {
    int id;
    int iq;
    String name;

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", iq=" + iq +
                ", name='" + name + '\'' +
                '}';
    }

    public Person(int id, int iq, String name) {
        this.id = id;
        this.iq = iq;
        this.name = name;
    }
}
