package files;

import java.io.*;
//объекты
public class FileTest3 {
    public static void main(String[] args) {
        Person person = new Person(12324, 99, "Рустем");
        Person person1 = new Person(123421, 102, "Андрей");
// записываем в файл
        File file = new File("C:\\Users\\hasis\\OneDrive\\desktop\\FileTest3\\FileTest3.txt");
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);

            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(person);
            objectOutputStream.writeObject(person1);
            fileOutputStream.close();
            objectOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
          FileInputStream  fileInputStream = new FileInputStream(file);
            ObjectInputStream objectOutputStream = new ObjectInputStream(fileInputStream);
            Person person11 = (Person) objectOutputStream.readObject();
            Person person22 = (Person) objectOutputStream.readObject();
            System.out.println(person11.toString());
            System.out.println(person22.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


    }
}
